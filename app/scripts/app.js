'use strict';

/**
 * @ngdoc overview
 * @name it327App
 * @description
 * # it327App
 *
 * Main module of the application.
 */
angular

.module('it327App', [
	'ngResource',
	'ui.router',
	'angular-gestures',
	'ngAnimate'
])

.config(['$stateProvider', '$urlRouterProvider', 'hammerDefaultOptsProvider',
	function ($stateProvider, $urlRouterProvider, hammerDefaultOptsProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'views/main.html',
				controller: 'MainCtrl'
			});
			hammerDefaultOptsProvider.set({
				// Add recognizers here:
				recognizers: [[Hammer.Pan, {time: 250}]]
			});
	}
])

.run(['$rootScope','$http',
	function ($scope, $http) {
		// App version info
		$http.get('version.json').success(function (v) {
			$scope.version = v.version;
			$scope.appName = v.name;
		});
	}
]);
