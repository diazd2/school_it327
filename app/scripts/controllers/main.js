'use strict';

/**
 * @ngdoc function
 * @name it327App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the it327App
 */
angular.module('it327App')
.controller('MainCtrl', [
	'$scope',
	'$timeout',
	function ($scope, $timeout) {
		$scope.lines = [];
		$scope.binaryChar = [];
		$scope.parityType = 1; // 0: even 1s = 0
		$scope.invert = false;
		$scope.errorsToAdd = 0;
		$scope.lastCRCInput = '';
		var offsetX = 50, sizeX = 40, sizeY = 40, svgWidth = 550, CRCTable = null;

		var buildCRCTable = function(){
			var c, crcTable = [];
			for(var n = 0; n < 256; n++){
				c = n;
				for(var k = 0; k < 8; k++){
					c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
				}
				crcTable[n] = c;
			}
			return crcTable;
		};
		CRCTable = buildCRCTable();

		var crc8 = function(strIn) {
			var i, j, crc = 0, polySeed = 0x07;
			for (j = 0; j < strIn.length; j++) {
				crc ^= strIn.charCodeAt(j);
				for(i = 0; i < 8; i++) {
					crc = (crc & 0x80)? ((crc << 1) ^ polySeed) : (crc <<= 1);
				}
				crc &= 0xFF;
			}
			return crc;
		};

		var crc32 = function(strIn) {
			var crcTable = CRCTable || (CRCTable = buildCRCTable());
			var crc = 0 ^ (-1);
			for (var i = 0; i < strIn.length; i++ ) {
				crc = (crc >>> 8) ^ crcTable[(crc ^ strIn.charCodeAt(i)) & 0xFF];
			}
			return (crc ^ (-1)) >>> 0;
		};

		var calcParity = function(binChar){
			var i = 0;
			var counter = 0;
			for(; i < binChar.length; i++) {
				counter += (binChar[i] === '1')? 1 : 0;
			}
			return ((counter + $scope.parityType) % 2);
		};

		var getLines = function(character, parity){
			character += parity + '0';
			var bitNumber = 1;
			var direction = -1, lastBit = 1; // 1: down, -1: up
			var lines = [];
			// start lines
			lines.push({
				x1: 0, y1: 0,
				x2: offsetX, y2: 0
			});
			lines.push({
				x1: offsetX, y1: 0,
				x2: offsetX, y2: (direction * sizeY)
			});
			lines.push({
				x1: offsetX, y1: (direction * sizeY),
				x2: offsetX + sizeX, y2: (direction * sizeY)
			});
			// char lines
			for(var j = 0; j < character.length; j++) {
				if(parseInt(character[j]) !== lastBit) {
					lines.push({
						x1: offsetX + (sizeX * bitNumber), y1: (direction * sizeY),
						x2: offsetX + (sizeX * bitNumber), y2: (direction * -1 * sizeY)
					});
					direction *= -1;
				}
				lines.push({
					x1: offsetX + (sizeX * bitNumber), y1: (direction * sizeY),
					x2: offsetX + (sizeX * (++bitNumber)), y2: (direction * sizeY)
				});
				lastBit = parseInt(character[j]);
			}
			// stop lines
			lines.push({
				x1: offsetX + (sizeX * bitNumber), y1: (direction * sizeY),
				x2: offsetX + (sizeX * bitNumber), y2: 0
			});
			lines.push({
				x1: offsetX + (sizeX * bitNumber), y1: 0,
				x2: svgWidth, y2: 0
			});
			return lines;
		};

		var calcErrorPositions = function(){
			$scope.errorsToAdd = ($scope.errorsToAdd > 8)? 8 : $scope.errorsToAdd;
			$scope.errorsToAdd = parseInt($scope.errorsToAdd) || 0;
			$scope.errorsArray = [];
			if($scope.errorsToAdd && $scope.errorsToAdd > 0) {
				var errorsSet = new Set();
				while(errorsSet.size < $scope.errorsToAdd) {
					errorsSet.add(parseInt(Math.random() * 8));
				}
				errorsSet.forEach(function(val){
					$scope.errorsArray.push(val);
				});
			}
		};

		var calcEncoding = function(character){
			if(character.length <= 6) {
				return 0; // 0 = ASCII
			} else if (character.length > 6 && character.length <= 8) {
				return 1; // 1 = ASCIIext
			} else if (character.length > 8) {
				return 2; // 2 = UTF-8 or more (invalid)
			}
		};

		$scope.goTo = function(page){
			$scope.page = page;
		};

		$scope.toggleInvert = function(){
			$scope.invert = !$scope.invert;
			if($scope.character){
				$scope.drawChar();
			}
		};
		$scope.toggleParity = function(){
			$scope.parityType = ($scope.parityType === 1)? 0 : 1;
			if($scope.character){
				$scope.drawChar();
			}
		};

		$scope.drawChar = function(manualChar){
			if(manualChar) {
				$scope.character = manualChar;
			}
			if($scope.character && $scope.character.length < 2) {
				$scope.character = $scope.character[0];
				// calc binary values and parity
				$scope.lines = [];
				var tmpChar = $scope.character.charCodeAt(0).toString(2);
				var encoding = calcEncoding(tmpChar);
				for(var j = 0, length = tmpChar.length; j < (8-length); j++){
					tmpChar = '0' + tmpChar;
				}
				if($scope.invert) {
					var tmpInverted = '';
					for(var i = 0; i < tmpChar.length; i++){
						tmpInverted += (tmpChar[i] === '1')?'0':'1';
					}
					tmpChar = tmpInverted;
				}
				var parityBit = calcParity(tmpChar);
				var tmpBitArray = [];
				for(j = 0; j < tmpChar.length; j++) {
					tmpBitArray.push({'bit': tmpChar[j]});
				}
				$scope.binaryChar = {
					'char': $scope.character,
					'char10': $scope.character.charCodeAt(0).toString(10),
					'char16': $scope.character.charCodeAt(0).toString(16),
					'bits': tmpBitArray,
					'parity': parityBit,
					'encoding': encoding
				};
				$scope.lines = getLines(tmpChar, $scope.binaryChar.parity);
			} else {
				if($scope.character && $scope.character.length > 1) {
					$scope.character = $scope.character[1];
					$scope.drawChar();
				} else {
					$scope.lines = null;
				}
			}
		};

		$scope.send = function(){
			if(!$scope.character || !$scope.character.length || $scope.binaryChar.encoding === 2) {
				$scope.receivedChar = [];
				$scope.receivedLines = [];
				return;
			}
			calcErrorPositions();
			$scope.receivedChar = angular.copy($scope.binaryChar);
			if($scope.errorsToAdd === 0){
				$scope.receivedChar.parityState = 0; // 0 = no errors
			} else if($scope.errorsToAdd % 2 === 0) {
				$scope.receivedChar.parityState = 2; // 2 = error not captured
			} else {
				$scope.receivedChar.parityState = 1; // 1 = error captured
			}
			var binChain = '', result = '';
			for(var i = 0; i < $scope.receivedChar.bits.length; i++) {
				if($scope.errorsArray.indexOf(i) >= 0) {
					$scope.receivedChar.bits[i].bit = ($scope.receivedChar.bits[i].bit === '1')?'0':'1';
					$scope.receivedChar.bits[i].hasError = true;
				}
				if($scope.receivedChar.bits[i].bit === '1') {
					result += ($scope.invert)?'0':'1';
				} else {
					result += ($scope.invert)?'1':'0';
				}
				binChain += $scope.receivedChar.bits[i].bit;
			}
			$scope.receivedChar.char = String.fromCharCode(parseInt(result, 2));
			$scope.receivedChar.char16 = $scope.receivedChar.char.charCodeAt(0).toString(16);
			$scope.receivedChar.char10 = $scope.receivedChar.char.charCodeAt(0).toString(10);
			$scope.receivedChar.encoding = calcEncoding($scope.receivedChar.char);
			if($scope.receivedChar.char10 >= 127 && $scope.receivedChar.char10 <= 160) {
				$scope.receivedChar.encoding = 2; // invalid
			}
			$scope.receivedChar.parity = calcParity(binChain);
			$scope.receivedLines = getLines(binChain, $scope.receivedChar.parity);
		};

		$scope.calcCRC = function(){
			if($scope.crcStrIn && $scope.crcStrIn.length > 0) {
				$scope.crc = {};
				$scope.crc.input = {
					hash8: '0x' + crc8($scope.crcStrIn).toString(16),
					hash32: '0x' + crc32($scope.crcStrIn).toString(16)
				};
				$scope.lastCRCInput = $scope.crcStrIn;
				$timeout(function(){
					$scope.sendCRC();
				}, 800);
			}
		};

		$scope.sendCRC = function(){
			if($scope.crcStrIn && $scope.crcStrIn.length > 0) {
				/* add error */
				$scope.crcStrOut = '';
				var pos = Math.floor(Math.random() * $scope.crcStrIn.length);
				var error = Math.floor(Math.random() * 94) + 33; // error = random letter
				var parts = {};
				var tmp = parts.left = $scope.crcStrIn.substring(0, pos);
				tmp += parts.error = String.fromCharCode(parseInt(error, 10));
				if(tmp.length < $scope.crcStrIn.length) {
					tmp += parts.right = $scope.crcStrIn.substring(pos + 1, $scope.crcStrIn.length);
				}
				$scope.crcStrOut = tmp;
				$scope.crc = {
					input: $scope.crc.input,
					output: {
						hash8: '0x' + crc8($scope.crcStrOut).toString(16),
						hash32: '0x' + crc32($scope.crcStrOut).toString(16),
						parts: parts
					}
				};
			}
		};

		$scope.clearCRC = function(){
			if(!$scope.crcStrIn || $scope.lastCRCInput !== $scope.crcStrIn) {
				$scope.crc = {};
			}
		};

		/* init */
		$scope.drawChar('A');
	}
]);
